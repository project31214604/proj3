const express = require('express');
const cors = require('cors')

const routePerson = require('./routes/person')
const app = express();
app.use(cors('*'))
app.use(express.json())

app.use('/person',routePerson)

app.listen(4500,'0.0.0.0', () =>{
    console.log('server started successfully on port 4500');
});